<p align="center"><img src="https://images.gitee.com/uploads/images/2020/1107/150426_914f2564_5451455.png" alt="SenWeather" /></p>

<h1 align="center">SenWeather</h1>

<p align="center"><a href='https://gitee.com/get-fan/SenWeather/stargazers'><img src='https://gitee.com/get-fan/SenWeather/badge/star.svg?theme=white' alt='star'></img></a><a href='https://gitee.com/get-fan/SenWeather/members'><img src='https://gitee.com/get-fan/SenWeather/badge/fork.svg?theme=white' alt='fork'></img></a></p>

<p align="center" href="https://qm.qq.com/cgi-bin/qm/qr?k=JZKRI6nauodLGZCDscrT4GeN81NQFe2E&jump_from=webapi"><img src="https://images.gitee.com/uploads/images/2020/1030/202406_afbe4fbf_5451455.png" /><img src="https://images.gitee.com/uploads/images/2020/1030/204134_78383053_5451455.png" /></p>

## ✨简介

SenWeather，开源免费的天气插件，接口来自心知天气，让你的Typecho博客加上美观的天气服务。

QQ交流群：939273209（团队技术人员专业在线答疑，免费咨询typecho及web开发的任何问题，允许打广告）



## ✅使用

1.下载插件

![下载插件](https://images.gitee.com/uploads/images/2020/1030/204229_13764e2f_5451455.png "image-20201030193120044.png")

2.安装插件

- 进入二级目录，将文件拖入到<code>usr\plugins</code>中

![安装插件](https://images.gitee.com/uploads/images/2020/1030/204315_9cf097dd_5451455.gif "GIF.gif")

- 进入typecho后台，启用插件

![启用插件](https://images.gitee.com/uploads/images/2020/1030/204406_8b563014_5451455.png "image-20201030194219184.png")

## 💟特色

超级美观的主题样式（持续开发中...）

![1](https://images.gitee.com/uploads/images/2020/1030/204835_4204046b_5451455.png "1.png")
![2](https://images.gitee.com/uploads/images/2020/1030/204848_793af8a2_5451455.png "2.png")
![3](https://images.gitee.com/uploads/images/2020/1030/204900_820b5306_5451455.png "3.png")
![4](https://images.gitee.com/uploads/images/2020/1030/204915_61b32127_5451455.png "6.png")
![5](https://images.gitee.com/uploads/images/2020/1030/204926_49e52a53_5451455.jpeg "4.jpg")
![6](https://images.gitee.com/uploads/images/2020/1030/204937_d7645971_5451455.jpeg "5.jpg")



## 💥功能

使用心知天气服务接口实现天气显示功能

## 💫说明

- 本插件使用国际GPL2.0开源协议，请详阅[开源协议](LICENSE)
- 如果遭遇bug或想要提想法，请到[lssues](https://gitee.com/get-fan/SenWeather/issues)中新建一个lssues
- 请为本项目加一个Star，欢迎有共同爱好的人Fork仓库参与代码贡献

## 💖贡献

- XiaoFans

