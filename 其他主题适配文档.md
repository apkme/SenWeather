# SenWeather插件适配文档

注意：本插件版本在v1.2.2及以上的版本中可以使用

1.锚点代码

```html
<div id="SenAnchor"></div>
```

在需要显示插件位置处插入锚点代码，其目的是插件会在该锚点之前显示

2.插件样式调整

```HTML
<div id="tp-weather-widget" class="navbar-form navbar-form-sm navbar-left shift"></div>
```

该段代码是插件会自动生成到锚点代码之前的代码，请根据css相关知识进行样式上的调整

```css
navbar-form navbar-form-sm
```

使用bootstrap框架的主题会自动对齐，没有使用的请自行修改class调整